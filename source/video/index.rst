.. _doc-android-video:

*****
Video
*****

**VLC for Android** is an excellent video player. It supports wide range of formats. It can also play animated GIFs and 360 videos. **VLC for Android** plethora of controls and features. Some of these features are :abbr:`PiP (Picture-in-Picture)` mode, subtitles, equalizer, rich sound, gesture controls, hardware acceleration and many more. The video explorer screen looks as below.

.. figure:: /images/video/video_explorer_video_ui.png
    :alt: Video Explorer Screen
    :align: center

Follow links below for different video features/settings.

.. toctree::
    :name: toc-android-video
    :maxdepth: 1

    video_explorer.rst
    video_player.rst
