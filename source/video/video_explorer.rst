.. _doc-video-explorer:

**************
Video Explorer
**************

.. role:: html-raw(raw)
    :format: html

This is the default view of **VLC for Android**. Here you can arrange your videos in grid/lists, create a playlist, delete, share etc. 
The Explorer View is as below.

.. figure:: /images/video/video_explorer_video_ui.png
    :alt: Video Explorer Screen
    :align: center

.. |search_icon| image:: /images/icons/search.svg
    :width: 3%

.. |resume_play| image:: /images/icons/ic_shortcut_resume_playback.svg
    :width: 4%

.. |kebab_menu| image:: /images/icons/ic_more.svg
    :width: 5%

.. |sort_icon| image:: /images/icons/ic_sort.svg
    :width: 5%

.. |list_icon| image:: /images/icons/ic_list.svg
    :width: 5%

.. |grid_icon| image:: /images/icons/ic_grid.svg
    :width: 5%

.. |cast_icon| image:: /images/icons/ic_player_renderer.svg
    :width: 5%

.. |cast_icon_on| image:: /images/icons/ic_player_renderer_on.svg
    :width: 5%

At the bottom there are 5 tabs. By tapping on them you will be taken to their respective screen. At the top-right there are many icons 
Search Media (|search_icon|), Resume Playback (|resume_play|), Sort by (|sort_icon|), Display in grid/Display in list 
(|grid_icon|/|list_icon|) etc. You can see all options by tapping on More Options (|kebab_menu|) icon.

* |cast_icon|/|cast_icon_on| ::guilabel:`Wireless Cast` - If a wireless cast receiveing device(e.g Chromecast) is detected, you can tap on this icon to cast your videos to your TV.

* |search_icon| :guilabel:`Search Media` - Tapping on this icon will open a search bar, you can search media that are currently indexed by **VLC for Android**. 

* |resume_play| :guilabel:`Resume Playback` - Tapping this icon will resume last played video.  

* |sort_icon| :guilabel:`Sort by`  - You can sort your videos by *Name*, *Length*, *Recently Added*, and *Media Number*. Tap the option a second time to change the order(ascending/descending) of sorting.

* |grid_icon|/|list_icon| :guilabel:`Display in grid/Display in list` - Videos can be arranged either in list or grid. 

.. |grid_view| image:: /images/video/video_explorer_grid_view.png
    :align: middle

.. |list_view| image:: /images/video/video_explorer_list_view.png
    :align: middle

.. table::
    :align: center

    +-------------+-------------+
    |  Grid View  |  List View  |
    +-------------+-------------+
    | |grid_view| | |list_view| |
    +-------------+-------------+

* **Group videos** - You can group your videos by *Name* and by *Folder*. Grouping keeps videos of similar type together and thus makes searching easy. Tap *Do not group videos* to show videos individually.

* **Refresh** - This options refreshes the current media list.

:html-raw:`<hr>`

More Options
============

In both views, three information are shown with the video. These are *Title*, *Resolution*, and *Length of the Video*. When you tap 
*More Options* you will be shown different context menu depending on *Sort* and *Group*. Given below are all the options you get and how 
to use them. 

.. |play| image:: /images/icons/ic_am_play.svg
    :width: 5%

.. |play_all| image:: /images/icons/ic_ctx_play_all_normal.svg
    :width: 5%

.. |play_start| image:: /images/icons/ic_ctx_play_from_start_normal.svg
    :width: 5%

.. |play_as_audio| image:: /images/icons/ic_am_playasaudio.svg
    :width: 5%

.. |append| image:: /images/icons/ic_am_append.svg
    :width: 5%

.. |information| image:: /images/icons/ic_am_information.svg
    :width: 5%

.. |down_subtitle| image:: /images/icons/ic_ctx_download_subtitles_normal.svg
    :width: 5%

.. |insert_next| image:: /images/icons/ic_ctx_play_next_normal.svg
    :width: 5%

.. |add_playlist| image:: /images/icons/ic_am_addtoplaylist.svg
    :width: 5%

.. |delete| image:: /images/icons/ic_menu_delete.svg
    :width: 5%

.. |share| image:: /images/icons/ic_am_share_normal.svg
    :width: 5%

.. |add_video_group| image:: /images/icons/ic_add_to_group.svg
    :width: 5%

.. |auto_group| image:: /images/icons/ic_ctx_group_auto.svg
    :width: 5%

.. |mark_played| image:: /images/icons/ic_ctx_mark_as_played.svg
    :width: 5%

.. |mark_all_played| image:: /images/icons/ic_ctx_mark_all_as_played.svg
    :width: 5%

.. |edit_icon| image:: /images/icons/ic_edit.svg
    :width: 5%

* |play| **Play** - Play the current video.
* |play_start| **Play from start** - Play the current video from start instead of resuming.
* |play_all| **Play all** - Play all videos in a group.
* |play_as_audio| **Play as audio** - Play video as audio only.
* |append| **Append** - Append the video to current play queue (in PiP or Casting mode).
* |insert_next| **Insert next** - Insert the video just after the current video in current play queue.
* |information| **Information** - Shows information about the video file. It includes *File size*, *Video Codec*, *Language*, *Resolution*, *Frame rate*, *Audio Bitrate*, *Audio Codec*, *Audio Channels*, *Audio Sample Rate* and other info.
* |down_subtitle| **Download subtitles** - This option opens a subtitle downloader pop-up and tries to automatically search for subtitles for the current video. You can also manually search for subtitles by filling search parameters.
* |add_playlist| **Add to playlist** - This option adds current video/group to a new/existing playlist. 
* |delete| **Delete** - This option deletes the current video file.
* |share| **Share** - This option opens a pop-up to share the current video via Social Media, Email, Wi-Fi, bluetooth etc. 
* |add_video_group| **Add to video group** - This option adds the current video to a group.
* |auto_group| **Regroup automatically** - This option automatically add the current video to suitable group.
* |edit_icon| **Rename video group** - Renames the current video group.
* |delete| **Ungroup** - Ungroup the current group.
* |mark_played| **Mark as played** - This option will mark the current video as completed. If :guilabel:`Show seen video marker` option is checked under *Interface* settings, using this option will show the marker on video thumbnail.
* |mark_all_played| **Mark all as played** - This option will mark all videos in current group as completed.

:html-raw:`<hr>`

You can also see some or all of these options while in ActionMode. ActionMode is launched when you *tap and hold/long tap* individual video or group. See examples below.

.. |group_action| image:: /images/video/video_explorer_group_actions.png
    :align: middle

.. |video_action| image:: /images/video/video_explorer_video_actions.png
    :align: middle

.. table::
    :align: center

    +----------------+----------------+
    | |group_action| | |video_action| |
    +----------------+----------------+

These are just examples, there can be more or less options visible depending on your screen size and orientation, you can see all options 
by tapping More Options (|kebab_menu|) whenever available.
