.. _doc-video-player:

************
Video Player
************

.. role:: html-raw(raw)
    :format: html

When you play a video for the first time in **VLC for Android**, tips are shown for the video player controls.

.. figure:: /images/video/video_player_tips.png
    :align: center
    :alt: Video Player Controls

**VLC for Android** Video Player supports many gesture controls. In the figure above you can see three of them. 

.. |updown_gesture| image:: /images/icons/ic_gesture_swipe_vertically.svg
    :width: 5%

.. |rightleft_gesture| image:: /images/icons/ic_gesture_swipe_horizontally.svg
    :width: 10%

* |updown_gesture| :html-raw:`<strong style="color:orange">Brightness</strong>/<strong style="color:orange">Volume</strong>` - By swiping vertically on left/right edge of the screen, you can control *Brightness* and *Volume* respectively.

* |rightleft_gesture| :html-raw:`<strong style="color:orange">Seek</strong>` - By swiping horizontally on the screen you can seek video forward/backward.

Tap :html-raw:`<strong style="color:white;background-color:orange">Got it, dismiss this</strong>` to close :html-raw:`<strong style="color:orange">Video player tips</strong>` .

:html-raw:`<hr>`

Audio 
=====

.. |subtitle_audio| image:: /images/icons/ic_player_audiotrack.svg
    :width: 5%

.. |kebab_menu| image:: /images/icons/ic_more.svg
    :width: 5%

.. figure:: /images/video/video_player_audio_subtitle.png
    :alt: Audio and Subtitles
    :align: center

Change/Disable Audio Track
--------------------------

You can change audio/subtitles tracks and adjust audio/subtitles delay of a video by tapping on |subtitle_audio| icon at the bottom-left 
corner of the screen. It will open *Audio and Subtitles* window and only *Subtitles* window if there are no audio tracks in the video.

.. figure:: /images/video/video_player_audio_subtitle.png
    :alt: Audio and Subtitles
    :align: center

You can either *Disable* the audio or select from the available audio tracks by tapping on them. 

Adjust Audio Delay
------------------

To adjust the audio delay tap on *Options* (|kebab_menu|) and select *Audio delay*. You could now adjust the audio delay with the interactive widget by following steps.

.. figure:: /images/video/video_player_audio_delay.png
    :alt: Audio Delay widget
    :align: center

1. If you already know the delay in *milliseconds(ms)*, then use the :html-raw:`<strong>&#x27E8 &#x27E9</strong>` buttons to hasten/delay the audio by 50 milliseconds on each tap. Tap **RESET** to reset audio delay. 

2. If you do not know the delay then use the widget to adjust. There are two options **SOUND HEARD** and **SOUND SPOTTED**. You should tap these buttons in order you observe them. 

3. For example if you hear a sound but the video is yet to display that scene, Tap **SOUND HEARD** and then when you finally observe the scene for that particular sound, tap **SOUND SPOTTED**. The delay will be adjusted automatically. You can spot the sound using subtitles(If there is no subtitle delay), or by observing the scene.

4. If the above step does not works, then first tap **RESET** then try buttons in reverse order. 

:html-raw:`<hr>`

Subtitles
=========

Download/Set Subtitles
----------------------

In the same window you could see pane for *Subtitles*. You can either *Disable* the subtitle or select from the available subtitles 
tracks. If there are no subtitles you can use following steps to get one.

1. If you have already downloaded the subtitles or have them on your device. Tap *Options* (|kebab_menu|) in the subtitles pane and tap **Select subtitle file**. This will open a tiny file explorer. Navigate to the folder having the Subtitles and tap on it to load. You can easily identify a subtitles file by |subtitle_audio| icon as shown in the screenshot below. 

.. figure:: /images/video/video_player_subtitles_select.png
    :align: center
    :alt: Select Subtitles File

2. If you do not have a subtitle file, **VLC for Android** lets you download one from the same screen. To download tap *Options* (|kebab_menu|) in the subtitles pane and tap **Download subtitles**. **VLC for Android** will try to search for available subtitles from the internet using the video metadata. In that case you could see a list of available subtitles, tap on suitable one to download and load. 

.. |search_icon| image:: /images/icons/search.svg
    :width: 4%

3. If video metadata is incomplete or not correct then **VLC for Android** may or may not show available subtitles. Then you must search manually. To search manually tap |search_icon| and fill the search parameter. In general :html-raw:`<i style="color:orange">Name</i>` should work but fill :html-raw:`<i style="color:orange">Season, Episode</i>` for best results. Tap :html-raw:`<strong style="color:orange">SEARCH</strong>` to start the search. You could now see a list of subtitles, select the suitable one to download and load.

4. Tap on *Subtitle History* icon to see previously used/downloaded subtitles.

:html-raw:`<hr>`

The *Globe* icon in the *Download Subtitles* widget can be used to filter subtitles by language. Tap on the globe icon to select a language. By default the app language is checked. You can check one or more languages. 

:html-raw:`<hr>`

Adjust Subtitles Delay
----------------------

.. figure:: /images/video/video_player_subtitles_delay.png
    :alt: Subtitles Delay Widget
    :align: center

You could also adjust Subtitles delay. Tap *Options* (|kebab_menu|) in the subtitles pane and select **Subtitle delay**. You could now adjust the subtitle delay using interactive widget by following steps.

1. If you already know the delay in *milliseconds(ms)*, then use the :html-raw:`<strong>&#x27E8 &#x27E9</strong>` buttons to hasten/delay the subtitle by 50 milliseconds on each tap. Tap **RESET** to reset subtitle delay. 

2. If you do not know the delay then use the widget to adjust. There are two options **VOICE HEARD** and **TEXT SEEN**. You should tap these buttons in order you observe them. 

3. For example if you hear a voice but the subtitle is yet to be displayed, Tap **VOICE HEARD** and then when you finally see the subtitle for that particular voice, tap **TEXT SEEN**. The delay will be adjusted automatically. You can spot the voice by observing the scene.

4. If the above step does not works, then first tap **RESET** then try buttons in reverse order.

:html-raw:`<hr>`

.. |landscape| image:: /images/icons/ic_player_lock_landscape.svg
    :width: 5%

.. |portrait| image:: /images/icons/ic_player_lock_portrait.svg
    :width: 5%

.. |rotate| image:: /images/icons/ic_player_rotate.svg
    :width: 5%

|landscape|/|rotate|/|portrait| **Screen Orientation** - Tap this icon to lock into current orientation or remove orientation lock. 

:html-raw:`<hr>` 

Screen Ratio
============

.. |screen_ratio| image:: /images/icons/ic_player_ratio.svg
    :width: 5%

|screen_ratio| **Screen Ratio** - **VLC for Android** lets you resize your video to best fit screen size/appearance. Given below are possible screen ratios. Tap this icon multiple time to select your preferred screen ratio.

.. table::
    :align: center

    +---------------------+--------------------------------------------------------------------------------------------+
    | Ratios              | Description                                                                                |
    +=====================+============================================================================================+
    | Best fit(*default*) | Automatically choose best screen ratio.                                                    |
    +---------------------+--------------------------------------------------------------------------------------------+
    | Fit Screen          | Stretch the video to fit to screen without distorting video. May hide some parts of video. |
    +---------------------+--------------------------------------------------------------------------------------------+
    | Fill                | Stretch the video to fill the screen without hiding parts. May distort the video.          |
    +---------------------+--------------------------------------------------------------------------------------------+
    | 16:9                | Stretch the video to fit 16:9 standard screen ratio.                                       |
    +---------------------+--------------------------------------------------------------------------------------------+
    | 4:3                 | Stretch the video to fit 4:3 standard screen ratio.                                        |
    +---------------------+--------------------------------------------------------------------------------------------+
    | Center              | Stretch the video to center of the screen.                                                 |
    +---------------------+--------------------------------------------------------------------------------------------+

:html-raw:`<hr>`

More Options
============

Tap **Options** icon at bottom-right corner of the screen to access more player controls/features. 

.. figure:: /images/video/video_player_more_options.png
    :alt: More Options
    :align: center

:html-raw:`<hr>`

.. |lock_icon| image:: /images/icons/ic_lock_player.svg
    :width: 5%

|lock_icon| :guilabel:`Lock` - This disables all video player controls and gestures to avoid accidental tap on the screen. After enabling 
this feature, when you tap on screen you could see a slide to unlock your screen. 

:html-raw:`<hr>`

.. |sleep_timer| image:: /images/icons/ic_sleep.svg
    :width: 5%

|sleep_timer| :guilabel:`Sleep timer` - You can set a sleep timer to let **VLC for Android** automatically close video playback after 
given period of time. 

.. figure:: /images/video/video_player_sleep_timer.png
    :align: center
    :alt: Sleep Timer

:html-raw:`<hr>`

.. |playback_speed| image:: /images/icons/ic_speed.svg
    :width: 5%

|playback_speed| :guilabel:`Playback speed` - You can set playback speed by using the slider, or by using arrows in the range 0.25x to 4x.

.. figure:: /images/video/video_player_playback_speed.png
    :alt: Video Playback Speed
    :align: center

:html-raw:`<hr>` 

.. |jump_time| image:: /images/icons/ic_jumpto.svg
    :width: 5%

|jump_time| :guilabel:`Jump to time` - This feature lets you jump to arbitrary time in the video. Alternatively you can also seek using 
gestures or video progress bar.

.. figure:: /images/video/video_player_jump_time.png
    :alt: Jump To Time
    :align: center

:html-raw:`<hr>`

.. |equalizer| image:: /images/icons/ic_equalizer.svg
    :width: 5%

|equalizer| :guilabel:`Equalizer` - **VLC for Android** embeds a powerful audio equalizer to customize your audio experience. By 
default the equalizer is disabled. To enable it, toggle the button at top-right corner. There are a lots of presets included and you can also 
create your own preset. 

.. figure:: /images/video/video_player_audio_equalizer.png
    :align: center
    :alt: Equalizer

:html-raw:`<hr>`

.. |pop_up_player_icon| image:: /images/icons/ic_popup_dim.svg
    :width: 5%

|pop_up_player_icon| :guilabel:`Pop-Up player` - This launches video in Picture-in-Picture mode. To customize pop-up player see :guilabel:`Use custom Picture-in-Picture popup` in :ref:`doc-extra-video` settings. 

.. |pop_up_player| image:: /images/video/video_player_popup_player.png

.. |pop_up_control| image:: /images/video/video_player_popup_player_control.png

.. table:: 
    :align: center
    :widths: 40 40

    +-----------------+------------------+
    | Pop-Up Player   | Pop-Up Controls  |
    +=================+==================+
    | |pop_up_player| | |pop_up_control| |
    +-----------------+------------------+

If you tap on the pop-up player, some controls will show up as shown in second screenshot. 

* Bottom three icons are play/pause and switch video controls. 
* The center square icon will exit the pop-up player and open the regular video player.
* The gear icon at top-left opens *PiP Mode Permission* in device settings. This must be enabled for pop-up player to work.
* Tap the cross icon at top-right to close the pop-up player completely. 

:html-raw:`<hr>`

.. |auto_repeat| image:: /images/icons/ic_auto_repeat.svg
    :width: 5%

.. |auto_repeat_1| image:: /images/icons/ic_auto_repeat_one.svg
    :width: 5%

|auto_repeat|/|auto_repeat_1| :guilabel:`Repeat mode` - Repeats the current video indefinitely.

:html-raw:`<hr>`

.. |information| image:: /images/icons/ic_am_information.svg
    :width: 5%

|information| :guilabel:`Video information` - Shows the current video information. An example is shown below. 

.. figure:: /images/video/video_player_video_information.png
    :align: center
    :alt: Big Buck Bunny Video Information

:html-raw:`<hr>`

.. |ab_repeat| image:: /images/icons/ic_abrepeat.svg
    :width: 5%

.. |ab_repeat_reset| image:: /images/icons/ic_abrepeat_reset.svg
    :width: 5%

.. |ab_marker| image:: /images/icons/ic_abrepeat_marker.svg
    :width: 5%

.. |ab_marker_reset| image:: /images/icons/ic_abrepeat_reset_marker.svg
    :width: 5%

|ab_repeat| :guilabel:`A-B repeat` - A-B repeat is to loop a video between two points. To start an A-B repeat follow these steps.

* Tap |ab_repeat| :html-raw:`<strong style="color:orange">SET START POINT</strong>` to set a start marker |ab_marker| at the time you want to start this repeat.

.. figure:: /images/video/video_player_abrepeat_start.png
    :alt: Start AB Repeat
    :align: center

* Now tap |ab_repeat| :html-raw:`<strong style="color:orange">SET END POINT</strong>` to set an end marker |ab_marker| at the time you want to end this repeat. Once set the *A-B repeat* will start. 

.. figure:: /images/video/video_player_abrepeat_end.png
    :alt: End AB Repeat
    :align: center

* Tap on |ab_marker_reset| to remove markers.
* Tap on |ab_repeat_reset| to close *A-B repeat*

.. figure:: /images/video/video_player_abrepeat.png
    :alt: AB Repeat
    :align: center

:html-raw:`<hr>`

:guilabel:`Save Playlist` - Add this video to an existing/new playlist.

.. figure:: /images/video/video_player_save_playlist.png
    :alt: Save Playlist
    :align: center
