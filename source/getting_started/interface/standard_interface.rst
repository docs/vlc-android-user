.. _doc-standard-interface:

*************************************************
VLC for Android interface for Smartphones/Tablets
*************************************************

The first launch of VLC for Android shows welcome screen and some initial setup. The following 
user interface applies to all touch devices(i.e. smartphones, tablets) by default. The scaling 
may be different but the overall user interface remains same with exactly same features and 
functions.

1. On first launch you will be greeted by **VLC for Android**. Tap the arrow at bottom-right corner.

.. figure:: /images/getting_started/interface/standard_interface_screen1.png
    :align: center
    :alt: The VLC welcome screen

2. On next screen you will be asked to grant permission to access media on your device. You may 
deny it. But remember without this permission **VLC for Android** would not be able to play your 
media. If you deny it for now, you will be asked again after next launch. Tap on :guilabel:`Allow`.

.. figure:: /images/getting_started/interface/standard_interface_screen2.png
    :align: center
    :alt: The VLC permission screen

3. The next screen gives you three option to control how **VLC for Android** discovers your media files.

.. figure:: /images/getting_started/interface/standard_interface_screen3.png
    :align: center
    :alt: Discovery Options

Checking first options will let **VLC for Android** automatically scan for new or deleted media at application startup. The second option 
controls which folders **VLC for Android** should scan for media, default is whole device. You can later change these in Settings. 

4. The final screen let you choose the default theme for **VLC for Android**. There are three types of theme 
offered by VLC. 

.. figure:: /images/getting_started/interface/standard_interface_screen4.png
    :align: center
    :alt: The VLC theme screen
 
* :guilabel:`DayNight mode` - This option will automatically switch themes from *Black* to *Light* and vice-versa as per time of the day.
* :guilabel:`Light theme` - Change the app theme to *Light*. Light themes are easier to see in bright environments.
* :guilabel:`Black theme` - Change the app theme to *Black*. Black themes are easier to eye in dark environments. It can also save battery on :abbr:`OLED (Organic Light Emitting Diode)` screen devices.

You can later change themes in the Interface Settings. Tap :guilabel:`DONE` and **VLC for Android** is ready for use.
