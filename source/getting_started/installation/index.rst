.. _doc-android-installation:

************
Installation
************

**VLC for Android** supports various screen sizes. They all can be broadly categorized into 
4 main groups. They are smartphone, tablets, televisions, and chromebooks. The single :abbr:`APK (Android Package)` 
can be installed to all of these Android/Chrome OS devices. Please tap on the desired device icon for instructions 
to install **VLC for Android** to them. 

.. |smartphone| image:: /images/getting_started/installation/index_smartphone.png
    :scale: 100%
    :target: standard_install.html

.. |tablet| image:: /images/getting_started/installation/index_tablet.png
    :scale: 100%
    :target: standard_install.html

.. |television| image:: /images/getting_started/installation/index_television.png
    :scale: 100%

.. |chromebook| image:: /images/getting_started/installation/index_chromebook.png
    :scale: 100%


.. table::
    :align: center

    +--------------+--------------+
    | |smartphone| |   |tablet|   |
    +--------------+--------------+
    |  Smartphones |    Tablets   |
    +--------------+--------------+
    | |television| | |chromebook| |
    +--------------+--------------+
    |  Televisions |  Chromebooks |
    +--------------+--------------+


.. toctree::
    :name: toc-android-installation
    :hidden:

    standard_install.rst
