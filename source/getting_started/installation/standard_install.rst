.. _doc-standard-install:

*************************************************
How to Install VLC in Android Smartphones/Tablets
*************************************************
There are many ways to install **VLC for Android** on your Android smartphones and tablets. The most reliable 
methods are described below. It is always better to install from officially recognized and authentic sources to 
avoid risk of malware and spyware.

From Google Play Store(Recommended)
===================================
Installing **VLC for Android** from **Google Play Store** is the simplest and most recommended method. Most of 
the android smartphones/tablets have *Google Play Store* preinstalled in them. Please follow the steps below 
to install VLC from Google Play Store.

1. Find :guilabel:`Play Store` in your application list. Many smartphones have all the applications on their 
home screen. Others may have *App Drawer* or *App Tray*. You can launch them by swiping up on your home screen 
or tapping some icon. You can find :guilabel:`Play Store` there. In case your smartphone/tablet does not have 
*Play Store*, you can try the alternative methods below. 

.. figure:: /images/getting_started/installation/standard_install_step1.png
    :align: center
    :alt: Play Store in Application List


2. In the *Play Store* tap to *Search Bar* at top and type *vlc for android* then tap the search icon. As you 
type you could already see :guilabel:`VLC for Android`  in the search results. Tap on it.

.. figure:: /images/getting_started/installation/standard_install_step2.png
    :align: center
    :alt: Search VLC in Play Store 


.. role:: raw-html(raw)
    :format: html

3. You should now see a screen similar to the one given below. Tap :guilabel:`Install` and wait for it to 
download and install. It may take few minutes depending upon your device and connection speed. Meanwhile, 
you could scroll down and read *About this app*, *Ratings and reviews* etc.

.. warning:: Please notice the app publisher name, it must be 
    :raw-html:`<strong style="color:#01875F";>Videolabs</strong>`. If its different please search again 
    and tap the one with the correct publisher. 

.. figure:: /images/getting_started/installation/standard_install_step3.png
    :align: center
    :alt: Install VLC 


4. Once installed you can tap :guilabel:`Open` to launch **VLC for Android** for the first time. Or you can 
exit the *Play Store* and launch it from your application list.  

.. figure:: /images/getting_started/installation/standard_install_step4.png
    :align: center
    :alt: Open VLC


.. warning:: It is always recommended to use *Play Store* for downloading and installing applications. The 
    *Play Store* automatically chooses best version of an app if there are multiple versions available. If for 
    some reasons you cannot use *Play Store* then only use alternative sources with precautions. Although all 
    the methods given here are well-tested, risks are unavoidable.

From F-Droid
============
Another popular source for android applications is `F-Droid <https://f-droid.org/>`_ repository. Here you can 
find older versions of an app as well should you have any need. However, please always install the latest version 
if not required otherwise. Please follow the steps given below to install VLC using **F-Droid**. 

.. warning:: You cannot upgrade the app installed from F-Droid to another source as they are compiling and signing
    the app themselves. If you want to upgrade you must use F-Droid again. Or you can uninstall the app and then 
    install from another source.

1. Open `https://f-droid.org/ <https://f-droid.org/>`_ in your favorite web browser(Chrome shown here).

.. figure:: /images/getting_started/installation/standard_install_alter1.png
    :align: center
    :alt: F-Droid Homepage


2. Scroll down and find the *Search Box*. Type *vlc* and tap the :guilabel:`Search`  icon. 

.. figure:: /images/getting_started/installation/standard_install_alter2.png
    :align: center
    :alt: Search VLC


3. Now you could see **VLC** in the search results. Tap on it. 

.. figure:: /images/getting_started/installation/standard_install_alter3.png
    :align: center
    :alt: Search Results


4. You can now see the page for **VLC**. There are information about the application. You may read them.

.. warning:: Please remember to choose the correct application from the search results. You can match the 
    icon and name from the screenshot given below for reference. 

.. figure:: /images/getting_started/installation/standard_install_alter4.png
    :align: center
    :alt: VLC App page


5. Scroll down to find the :guilabel:`Download APK` link. Please remember to download the latest version. If 
you need to install older versions. Scroll down further and find respective download link.

.. figure:: /images/getting_started/installation/standard_install_alter5.png
    :align: center
    :alt: The download link for VLC APK


6. You may get this warning after tapping on the download link. Tap :guilabel:`OK` and let the download complete.

.. figure:: /images/getting_started/installation/standard_install_alter6.png
    :align: center
    :alt: Confirm Download Warning


7. Once the APK file is downloaded, open the downloaded file. You can find this file either in your download list 
or you can use any *File Explorer* to go to your *Downloads* folder. After you tap on the file you may get a pop-up 
similar to as below. Tap :guilabel:`Install` .

.. figure:: /images/getting_started/installation/standard_install_alter7.png
    :align: center
    :alt: Install Pop-up

You may also get this pop-up if you are installing an application from external source for the first time. 

.. figure:: /images/getting_started/installation/standard_install_alter8.png
    :align: center
    :alt: Installed Blocked Popup

In this case tap :guilabel:`Settings` . And then you will be taken to the respective app permission setting. 
It looks similar to as the screenshot below. Toggle the switch to *ON*, or if there is checkbox please check 
the box.

.. figure:: /images/getting_started/installation/standard_install_alter9.png
    :align: center
    :alt: Grant installation permission.

Open the file again and proceed from the last step. 


8. You may get this error if the version you downloaded is not compatible with your device or the downloaded 
file is corrupted. In this case please try to download the file again or install a different version. 

.. figure:: /images/getting_started/installation/standard_install_error.png
    :align: center
    :alt: Installation Error


From VLC's Official Website
===========================

You can also download **VLC for Android** APK file from our official website. Here you can find every version of **VLC for Android** 
ever released as well as for each processor type. However, please always install the latest version if not required otherwise. Follow 
the instructions below to install from VLC's official website. 

1. Open `https://get.videolan.org/vlc-android/ <https://get.videolan.org/vlc-android/>`_ in your favorite web browser(Chrome shown here).

.. figure:: /images/getting_started/installation/standard_install_vlc1.png
    :align: center
    :alt: VLC's Official App repository

2. Scroll down to the bottom and tap on the last link. The last link denotes the latest version. You can also see the version number and its 
creation date to find the latest version. Or you may choose any version you need.

3. Now you can see 8 links. Four of them are APK files and another four are their respective checksums. VLC offers APK files for 4 types of 
processor architecture. 

.. table:: 
    :align: center
    :widths: 10 10 5 7

    +---------------+----------------+---------+------------+
    | **arm64-v8a** | **armebi-v7a** | **x86** | **x86_64** |
    +---------------+----------------+---------+------------+

.. figure:: /images/getting_started/installation/standard_install_vlc2.png
    :align: center
    :alt: VLC for Android versions. 

You should download a version according to your processor architecture. ARM64(arm64-v8a) is the most common processor architecture. It should 
work for most of the Android devices. 
   

From Huawei AppGallery (For Huawei Devices)
===========================================

Since Huawei devices do not come with *Google Play Store*, Huawei provides *AppGallery* as an alternative app store. 
Follow the instructions below to install **VLC for Android** from *Huawei AppGallery*. 

1. Find :guilabel:`AppGallery` in your application list. Launch the app by tapping on it. 

.. figure:: /images/getting_started/installation/standard_install_huawei1.png
    :align: center
    :alt: AppGallery in Application List


2. Tap on the *Search Box* at the top and type *VLC* into the box. As you type you could already see :guilabel:`VLC for Android` 
in the results below. Tap on it. 

.. figure:: /images/getting_started/installation/standard_install_huawei2.png
    :align: center
    :alt: VLC in AppGallery


3. Now you could see the app page for **VLC for Android**. At the bottom you can see :guilabel:`INSTALL` button. Tap on it to start the 
downloading and installation process. It could take a while to download and install depending upon your device and connection speed. 
Download size could vary version to version.

.. figure:: /images/getting_started/installation/standard_install_huawei3.png
    :align: center
    :alt: Install Prompt in AppGallery

.. note:: In case you are not sure if you are installing the correct application. Tap on the *About* and verify the *Developer* and 
    *Provider* as **Videolabs**.

4. Once installed you can see the button changed to :guilabel:`OPEN`. Tap on it to launch **VLC for Android** for the first time. Or 
you can exit *AppGallery* and launch **VLC** from your application list.

.. figure:: /images/getting_started/installation/standard_install_huawei4.png
    :align: center
    :alt:  Open Prompt in AppGallery


From other sources
==================
Since number of *Android* applications are very large. There are numerous other sources to install an application. 
However the procedure remains similar to what described above. In case you are not able to install from any of the 
methods described above, you can use other sources too. Every Android device has a preinstalled app store if not 
*Google Play Store*. Please search for **VLC** in your default application store. Remember to match the name and icon 
of the application and avoid installing any app with different name and icon.
