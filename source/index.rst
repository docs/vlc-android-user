.. _doc-vlc-android-user:

**********************************
VLC for Android User Documentation
**********************************

.. table::
    :align: center

    +---------------------------------------+
    | :ref:`doc-android-getting-started`    |
    +---------------------------------------+
    | :ref:`doc-android-video`              |
    +---------------------------------------+
    | :ref:`doc-android-audio`              |
    +---------------------------------------+
    | :ref:`doc-android-browse`             |
    +---------------------------------------+
    | :ref:`doc-android-more`               |
    +---------------------------------------+
    | :ref:`doc-android-support`            |
    +---------------------------------------+




.. toctree::
    :maxdepth: 2
    :hidden:
    
    getting_started/index.rst
    video/index.rst
    audio/index.rst
    browse/index.rst
    more/index.rst
    support/index.rst