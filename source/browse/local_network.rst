.. _doc-local-network:

*************
Local Network
*************

.. role:: html-raw(raw)
    :format: html

**VLC for Android** supports many local network protocols. With help of this feature you can easily browse your network storage and 
stream media files over local network. If your device is connected to a local network and storage device on that network is visible then 
**VLC for Android** will automatically detect and show them below :html-raw:`<strong style=:color:orange">Local Network</strong>` 
section. You can then just tap and browse like a local storage and play media. You can always add a local network manually by tapping on 
the floating :html-raw:`<strong style="font-size:200%">+</strong>` button. To setup using different network protocols see respective 
sections.

SMB
===
1. Tap on the floating :html-raw:`<strong style="font-size:200%">+</strong>` button, then tap on the drop-down menu at top-left corner and select *SMB*. A popup will open.

.. figure:: /images/browse/browse_local_network_smb.png
    :align: center
    :alt: SMB Configure

2. Fill the network details. You must fill :html-raw:`<i style="color:orange">Network share name(or IP address)</i>` at least. Tap :html-raw:`<strong style="color:orange">OK</strong>` to save the configuration. 

3. The configuration will be added to the :html-raw:`<strong style="color:orange">Favorite</strong>` section. Tap on the configuration to connect.

4. If your SMB server is protected by a password you will be required to fill the credentials. Check *Remember password* to avoid filling every time you connect. Tap :html-raw:`<strong style="color:orange">OK</strong>` to connect.

.. figure:: /images/browse/browse_local_network_smb_auth.png
    :align: center
    :alt: SMB Authentication

5. Now you could browse the local network as any other storage and play media. All video/audio player controls are available in this mode too. An example is shown below. 

.. figure:: /images/browse/browse_local_network_smb_explore.png
    :align: center
    :alt: Network Browse

   