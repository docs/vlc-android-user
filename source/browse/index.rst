.. _doc-android-browse:

******
Browse
******

.. role:: html-raw(raw)
    :format: html

You can search your media files, storage media, and browse files on your local network. You can also mark folders and networks as favorite. Browse screen is shown in the screenshot below. 

.. figure:: /images/browse/browse_main_screen.png
    :alt: Browse main Screen
    :align: center

.. |kebab_menu| image:: /images/icons/ic_more.svg
    :width: 5%

.. |mark_favorite| image:: /images/icons/ic_bookmark_outline.svg
    :width: 5%

Favorites
=========

In the :html-raw:`<strong style="color:orange">Favorites</strong>` section you can put your favorite folders and local networks for faster access. To mark a folder/network favorite, tap on *Options* (|kebab_menu|) and then select |mark_favorite| :guilabel:`Add to favorites` . 

:html-raw:`<hr>`

Storages
========

In the :html-raw:`<strong style="color:orange">Storages</strong>` section you can browse your internal memory as well as external memory card (if inserted). There are lots of options to easily search and manage your media files in storage media. You can always access all options by tapping on |kebab_menu| *Options*. 

.. |search_icon| image:: /images/icons/search.svg
    :width: 3%

.. |sort_icon| image:: /images/icons/ic_sort.svg
    :width: 5%

.. |delete| image:: /images/icons/ic_menu_delete.svg
    :width: 5%

* |search_icon| **Search Media** - Search your files and folders in the current list. 
* |sort_icon| **Sort** - Sort the current list by *Name*.
* |mark_favorite| **Add/Remove from favorites** - Mark/Unmark the current folder as favorite.
* **Add to playlist** - Add this folder(and subfolders) to existing/new playlist.
* **Show all files** - Check to show all files(media and non-media).
* **Show hidden files** - Check to show hidden files.
* **Refresh** - Refresh the current list.
* |delete| **Delete** - Delete the current file/folder.

Local Network
=============

:ref:`doc-local-network` 

.. toctree::
    :name: toc-android-browse
    :hidden:

    local_network.rst