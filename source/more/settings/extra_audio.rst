.. _doc-extra-audio:

*****
Audio
*****

These settings are related to audio output during audio and video playback. 

.. role:: html-raw(raw)
    :format: html

.. figure:: /images/more/settings/extra_audio_general.png
    :align: center
    :alt: Audio Extra Settings

:guilabel:`Resume playback after a call` - **VLC for Android** pauses any media if a call is received during playback. Enabling this setting 
will resume audio playback after call is completed. 

:html-raw:`<hr>`

:guilabel:`Save Playback Speed` - Enabling this setting will keep playback speed the same after switching audio. 

:html-raw:`<hr>`

:guilabel:`Stop on application swipe` - In general applications in Android keep running in background until you dismiss the from recent application list.
Enabling this setting will stop audio playback when **VLC for Android** is dismissed from recent/running application list. 

:html-raw:`<hr>`

:guilabel:`Digital audio output (passthrough)` - Many modern smartphones comes with built-in support for enhanced audio output capabilities such as 
**Dolby Atmos**, **DTS:X** and other object-based surround sound technologies. Enabling this feature will allow **VLC for Android** to take advantage 
of such capabilities to enhance audio.

.. note:: The :guilabel:`Audio output` setting under `Advanced`_ below must be set to *AudioTrack* in 
    order to enable this setting.

Headset
=======

.. figure:: /images/more/settings/extra_audio_headset.png
    :align: center
    :alt: Audio Headset Settings

:guilabel:`Detect headset` - Enabling this setting lets **VLC for Android** detect the insertion and removal of headset. This can aid using 
play/pause functionality while insertion/removal or control playback using headset buttons.

:html-raw:`<hr>` 

:guilabel:`Resume on headset insertion` - In general **VLC for Android** pauses playback if the headset is removed. Enabling this settings lets 
**VLC for Android** resume playback headset is inserted while a playback is running.

.. note:: :guilabel:`Detect headset` option must be checked in order to use any functionality related to headsets including this feature.

Advanced
========

.. figure:: /images/more/settings/extra_audio_advanced.png
    :align: center
    :alt: Advanced Audio Settings

:guilabel:`Audio output` - **VLC for Android** uses two different API to render audio. 

.. figure:: /images/more/settings/extra_audio_advanced_output.png
    :align: center
    :alt: Audio output APIs

* AudioTrack is the built-in Android audio processing system that also supports manufacturer-installed add-on and audio tweaks. Choose this option if you want to use :guilabel:`Digital audio output`.

* :abbr:`OpenSL ES (Open Sound Library for Embedded Systems)` is a cross-platform, hardware-accelerated audio API for 2D and 3D audio.
