.. _doc-android-settings:

********
Settings
********

.. toctree::
    :name: toc-android-settings
    :maxdepth: 1

    general_settings.rst
    extra_interface.rst
    extra_video.rst
    extra_subtitles.rst
    extra_audio.rst
    extra_casting.rst
    extra_advanced.rst
