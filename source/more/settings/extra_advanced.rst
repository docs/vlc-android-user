.. _doc-advanced-settings:

********
Advanced
********

.. role:: html-raw(raw)
    :format: html

.. warning:: Please do not change any settings here until you know the aftereffect. The default settings are set for best optimization 
    and performance of the application. Bad setting may affect the stability and performance of application and device. 
    
.. figure:: /images/more/settings/extra_advanced_general.png
    :align: center
    :alt: Advanced settings

:guilabel:`Network caching value` - This is the time in milliseconds **VLC for Android** should buffer network media for software 
decoding. Caching network media can reduce buffering in slow network connection . A large caching value will result in larger waiting 
time for media playback. Clear this value to reset caching value.

.. figure:: /images/more/settings/extra_advanced_general1.png
    :align: center
    :alt: Network caching value

:html-raw:`<hr>`

:guilabel:`Dump media database` - This action copies current media database to internal storage root as :html-raw:`<b style="color:green">vlc_media.db</b>`. This file can be used later to restore your media database(such as playlists) into **VLC for Android** and sending it to *VLC Developers* for debugging purposes.

:html-raw:`<hr>`

:guilabel:`Clear media database` - This action clears any media database and create a new database. Tap :html-raw:`<b style="color:orange">YES</b>` on the pop-up to confirm this action. 

.. figure:: /images/more/settings/extra_advanced_general3.png
    :align: center
    :alt: Clear media database

.. caution:: You will lose all your progresses and created playlists after this action.

:html-raw:`<hr>`

:guilabel:`Clear playback history` - This action only clears your playback history. Tap :html-raw:`<b style="color:orange">YES</b>` on 
the pop-up to confirm this action. 

.. figure:: /images/more/settings/extra_advanced_general4.png
    :align: center
    :alt: Clear playback history

:html-raw:`<hr>`

:guilabel:`Quit and restart application` - This action stops any ongoing playbacks then force closes application and restarts it. 

:html-raw:`<hr>`

Performance
===========

.. figure:: /images/more/settings/extra_advanced_performance.png
    :align: center
    :alt: Advanced Performance Settings

:guilabel:`Time-stretching audio` - Whenever a media is played faster/slower than its normal playback speed, pitch changes. Enabling this 
option will keep the pitch unchanged while speeding/slowing a media. This may cause lag on slower device, prefer using only you have a 
fast device.

:html-raw:`<hr>`

:guilabel:`OpenGL ES2 usage` - *OpenGL for Embedded Systems* is a subset of *OpenGL* computer graphics rendering API  for rendering 2D 
and 3D computer graphics. By default **VLC for Android** automatically decides when to use this feature. You can force turn it ON/OFF.

.. figure:: /images/more/settings/extra_advanced_performance2.png
    :align: center
    :alt: OpenGL ES2 usage settings

:html-raw:`<hr>`

:guilabel:`Force video chroma` - Force video renderer to follow a particular color model. **VLC for Android** offers three options.

.. figure:: /images/more/settings/extra_advanced_performance3.png
    :align: center
    :alt: Video Chroma Setting

**RGB 32-bit** - (default) Best quality but performance may vary device-to-device.

**RGB 16-bit** - Better performance but video quality is reduced. 

**YUV** - Best performance but only works with with :abbr:`Android 2.3 (Android Gingerbread)` and later.

:html-raw:`<hr>`

:guilabel:`Deblocking filter settings` - Deblocking filter can improve visual quality. More deblocking means more improvement in visual 
quality but a slow performance. Less deblocking can provide significant speedup to high definition streams. **VLC for Android** provides 
four levels of deblocking, *No*, *Low*, *Medium*, *Full* and an *Automatic* option to let **VLC for Android** automatically decide 
deblocking level.

.. figure:: /images/more/settings/extra_advanced_performance4.png
    :align: center
    :alt: Deblocking filter settings

:html-raw:`<hr>`

:guilabel:`Frame skip` - This setting allows **VLC for Android** to skip frames while decoding. This could speed-up the decoding process 
but lowers video quality. You can enable this setting on a slow device to improve performance. 

:html-raw:`<hr>`

Developer
=========

.. figure:: /images/more/settings/extra_advanced_developer.png
    :align: center
    :alt: Advanced Developer Settings

:guilabel:`Verbose` - This setting increase verbosity of the logs produced/recorded by **VLC for Android**. This could help developers 
get more information about a crash/bug. 

:html-raw:`<hr>`

:guilabel:`Debug logs` - This setting launches the logcat where you can manually start logging **VLC for Android** events. 

.. figure:: /images/more/settings/extra_advanced_developer2.png
    :align: center
    :alt: Logcat screen

* **START LOGGING** - Start logging. You could see the logs below these buttons. You can keep logcat running and do your work. A notification also appears in the notification panel. Tap the notification to return to this screen.

* **STOP LOGGING** - Stop logging.

* **COPY TO CLIPBOARD** - Copy the current logs to the clipboard. All logs may or may not be copied to the clipboard, check maximum character capacity of your clipboard in that case.

* **DUMP LOGCAT LOG** - Save the current logs to storage root. The file will be named as :html-raw:`<strong style="color:green">vlc_logcat_yyyyMMdd_HHmmss.log</strong>` . This file can be sent to **VLC Developers** for debugging purposes.

* **CLEAR LOG** - Clear log on current screen. 

:html-raw:`<hr>`

:guilabel:`Custom libVLC options` - This option is used to supply advanced level commands to **VLC**. Only to be used for development purpose. 
