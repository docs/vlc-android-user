.. _doc-extra-subtitles:

*********
Subtitles
*********

These settings can be used to set various options for Subtitles. It includes color, font size, font weight, encoding etc. 

.. role:: html-raw(raw)
    :format: html

.. figure:: /images/more/settings/extra_subtitles_general.png
    :align: center
    :alt: Subtitles extra settings

:guilabel:`Auto load subtitles` - Enabling this option will auto load subtitles, if present/included in the media. 

:html-raw:`<hr>`

:guilabel:`Subtitles Size` - You can set your preferred Subtitles Font Size using this settings. There are 4 options available.

.. figure:: /images/more/settings/extra_subtitles_general_size.png
    :align: center
    :alt: Subtitles size options

.. table::
    :align: center
    :widths: 20 50

    +----------+------------------------------------------------------------------------------+
    | Size     | Effect                                                                       |
    +==========+==============================================================================+
    | Small    | :html-raw:`<p style="font-size:12px">This is a <i>small</i> subtitle.</p>`   |
    +----------+------------------------------------------------------------------------------+
    | Normal   | :html-raw:`<p>This is a <i>normal</i> subtitle.</p>`                         |
    +----------+------------------------------------------------------------------------------+
    | Big      | :html-raw:`<p style="font-size:16px">This is a <i>big</i> subtitle.</p>`     |
    +----------+------------------------------------------------------------------------------+
    | Huge     | :html-raw:`<p style="font-size:18px">This is a <i>huge</i> subtitle.</p>`    |
    +----------+------------------------------------------------------------------------------+

:html-raw:`<hr>`

:guilabel:`Subtitles Color` - You can set your preferred Subtitles Color using this setting. There are 6 options available.

.. figure:: /images/more/settings/extra_subtitles_general_color.png
    :align: center
    :alt: Subtitles color options

.. table::
    :align: center
    :widths: 20 50

    +----------+-----------------------------------------------------------------------------------------------------+
    | Color    | Effect                                                                                              |
    +==========+=====================================================================================================+
    | White    | :html-raw:`<p style="color:white; background-color:gray">This is a <i>white</i> subtitle.</p>`      |
    +----------+-----------------------------------------------------------------------------------------------------+
    | Gray     | :html-raw:`<p style="color:gray">This is a <i>gray</i> subtitle.</p>`                               |
    +----------+-----------------------------------------------------------------------------------------------------+
    | Pink     | :html-raw:`<p style="color:pink">This is a <i>pink</i> subtitle.</p>`                               |
    +----------+-----------------------------------------------------------------------------------------------------+
    | Blue     | :html-raw:`<p style="color:blue">This is a <i>blue</i> subtitle.</p>`                               |
    +----------+-----------------------------------------------------------------------------------------------------+
    | Yellow   | :html-raw:`<p style="color:yellow">This is a <i>yellow</i> subtitle.</p>`                           |
    +----------+-----------------------------------------------------------------------------------------------------+
    | Green    | :html-raw:`<p style="color:green">This is a <i>green</i> subtitle.</p>`                             |
    +----------+-----------------------------------------------------------------------------------------------------+

:html-raw:`<hr>`

:guilabel:`Subtitles Background` - Enabling this setting will show a translucent black background for subtitles. 

.. table::
    :align: center
    :widths: 20 20

    +----------------------------------------------------------------------------+------------------------------+
    | Enabled                                                                    | Disabled                     |
    +============================================================================+==============================+
    | :html-raw:`<p style="background-color:gray">Subtitles with background</p>` | Subtitles without background |
    +----------------------------------------------------------------------------+------------------------------+

:html-raw:`<hr>`

:guilabel:`Bold subtitles` - Enabling this setting will make the subtitles bold. 

.. table::
    :align: center
    :widths: 15 15

    +---------------------------------------------+------------------+
    | Enabled                                     | Disabled         |
    +=============================================+==================+
    | :html-raw:`<strong>Bold Subtitles</strong>` | Normal Subtitles |
    +---------------------------------------------+------------------+

:html-raw:`<hr>`

:guilabel:`Subtitle text encoding` - You can choose subtitles text encoding from this setting. Text encoding is a set of mappings between 
the bytes in the system and the characters in the character set. In general this is automatically decided by the application. In case your
subtitle is in different character set than Roman(English) alphabets, you may need to change this setting to appropriate encoding. The 
default option should work for most subtitles. **VLC for Android** supports all widely used text encoding.

.. figure:: /images/more/settings/extra_subtitles_general_encoding.png
    :align: center
    :alt: Subtitles encoding options
