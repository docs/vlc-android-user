.. _doc-general-settings:

****************
General Settings
****************

.. role:: html-raw(raw)
    :format: html

These are most common settings offered by **VLC for Android**. You can set options like media scan, history, orientation and enable features like PiP mode and hardware acceleration.

Media library
=============

.. figure:: /images/more/settings/general_settings_media_library.png
    :alt: Media Library Settings
    :align: center

:guilabel:`Media library folders` - Choose folder to be included in the **VLC for Android** media library. A file explorer will open 
after tapping this option. Check the box in front of folders to include them. There are three icons at the top-right corners. 

* **Search** - Search for a folder in the current list.

* **Sort** - Sort the list by *Name*. 

* **More Options** - :guilabel:`Add a custom path` Manually add a folder to scan. Please write fully qualified path name. E.g :html-raw:`<strong style="color:green">/storage/emulated/0/Movies</strong>` . 

.. note:: Once a folder is checked, its subfolders cannot be unchecked. To select only certain subfolders, first uncheck parent folder 
    and then select subfolders. *This is not a bug*.

:html-raw:`<hr>` 

:guilabel:`Auto rescan` - Check this option to enable automatic scan of device for new/deleted media at application startup.

:html-raw:`<hr>`

Video
=====

.. figure:: /images/more/settings/general_settings_video.png
    :alt: General Video Settings
    :align: center

:guilabel:`Background/PiP mode` - Select what **VLC for Android** should do when the application is switched from video playback.
 
.. figure:: /images/more/settings/general_settings_video1.png
   :alt: Background/PiP mode
   :align: center

* **Stop** - Stop playing video.

* **Play video in background** - Keep playing video in background.

* **Play video in Picture-in-Picture mode** - Play video in small screen overlay. Check PiP mode permission in device settings for **VLC** if this feature is not working. For devices earlier than Android 8.0 Oreo mark :guilabel:`Use custom Picture-in-Picture popup` option below :ref:`doc-extra-video` settings. 

:html-raw:`<hr>`

:guilabel:`Hardware Acceleration` - **VLC for Android** can take advantage of powerful GPU in modern devices to improve performance and 
quality of video playback. 

.. figure:: /images/more/settings/general_settings_video2.png
   :alt: Hardware Acceleration
   :align: center

* **Automatic** - (*default*) Let **VLC for Android** device when to use hardware acceleration.

* **Disabled** - Do not use hardware acceleration. Use this if you experience frequent instability in video playback especially on slower devices.

* **Decoding acceleration** - Use hardware acceleration for video decoding only.

* **Full acceleration** - Use full hardware acceleration. 

:html-raw:`<hr>`

:guilabel:`Video screen orientation` - Set default screen orientation during video playback.

.. figure:: /images/more/settings/general_settings_video2.png
   :alt: Video Screen orientation
   :align: center

* **Automatic(sensor)** - (*default*) Change screen orientation automatically using device sensors.

* **Locked at start** - Do not change orientation from what was at video start.

* **Landscape** - Always play videos in landscape mode.

* **Portrait** - Always play videos in portrait mode.

:html-raw:`<hr>`

History
=======

.. figure:: /images/more/settings/general_settings_history.png
   :alt: Playback History
   :align: center

:guilabel:`Playback history` - Check this option to save all media playback in History section. 
