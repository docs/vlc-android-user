.. _doc-extra-video:

*****
Video
*****

.. role:: html-raw(raw)
    :format: html

.. figure:: /images/more/settings/extra_video_general.png
    :align: center
    :alt: General Video Player Settings

:guilabel:`Save audio delay` - This setting controls if **VLC for Android** should save adjusted audio delays of individual videos. 

:html-raw:`<hr>` 

:guilabel:`Save brightness level` - Enabling this setting will keep the brightness level unchanged when you change videos. Disabling 
this will reset the brightness level to default after you change videos. 

:html-raw:`<hr>`

:guilabel:`Save Playback Speed` - Enabling this setting will keep the playback speed unchanged when you change videos. Disabling this 
will reset the playback speed to default(1.00x) after you change video.  

:html-raw:`<hr>`

:guilabel:`Fast seek` - Enabling this setting will increase the rate for fast forwarding when you seek videos sliding thumb across the screen. 
This is faster but the seeking will be less accurate. See the gesture :ref:`here <figure-video-tips>` .

:html-raw:`<hr>`

:guilabel:`Video transition` -  Enabling this setting will show video title and video player controls when new video in the playlist is played. 

:html-raw:`<hr>`

:guilabel:`Use custom Picture-in-Picture popup` - In general the video player sub-window size in :abbr:`PiP (Picture-in-Picture)` mode is fixed. 
Enabling this setting lets you resize the video player sub-window. You should must enable this setting to use PiP mode on devices with Android 
earlier than Android 8.0 Oreo. 

.. note:: For this feature to work you must enable PiP mode permissions from your device settings as well as :guilabel:`Background/PiP mode` under *Video* from 
    **VLC for Android** *Settings* main page. 

:html-raw:`<hr>`

:guilabel:`Resume played videos` - This setting controls if **VLC for Android** should resume played video. It offers three options.

.. figure:: /images/more/settings/extra_video_general7.png
    :align: center
    :alt: Resume options
   
.. |resumeplayback| image:: /images/more/settings/extra_video_general_resume.png
    :align: middle

.. table::
    :align: center

    +------------------------+--------------------------------------+
    | Options                | Description                          |
    +========================+======================================+
    | Always                 | Always resume played videos          |
    +------------------------+--------------------------------------+
    | Never                  | Do not resume played videos          |
    +------------------------+--------------------------------------+
    | Ask confirmation       | |resumeplayback|                     |
    +------------------------+--------------------------------------+

Controls
========

.. figure:: /images/more/settings/extra_video_controls.png
    :align: center
    :alt: Video Player Controls Settings

:guilabel:`Audio-boost` - This feature allows **VLC for Android** to boost device's default volume up to 200% while playing video. This is 
useful when you are watching video with a group. 

:html-raw:`<hr>`

:guilabel:`Volume gesture` - This enables the gesture to control volume during video playback by sliding fingers across right half of device 
screen. See the gesture :ref:`here <figure-video-tips>` .

:html-raw:`<hr>`

:guilabel:`Brightness gesture` - This enables the gesture to adjust brightness during video playback by sliding fingers across left half of 
device screen. 

.. _figure-video-tips:

.. figure:: /images/video/video_player_tips.png
    :align: center
    :alt: Video Player Gestures
   
:html-raw:`<hr>`

:guilabel:`Double tap to seek` - This feature lets you seek video by 10 seconds forward/backward by double tapping on right/left screen edges. 
You can also tap multiple times to seek more than 10 seconds such as, :menuselection:`3 Taps --> 20 seconds`, :menuselection:`4 Taps --> 30 seconds` 
and so on.   

:html-raw:`<hr>`

:guilabel:`Seek buttons` - Enabling this setting will show 10 seconds rewind/forward buttons in the video player interface. 

.. |enableseek| image:: /images/more/settings/extra_video_controls_seek2.png
    :align: middle

.. |disableseek| image:: /images/more/settings/extra_video_controls_seek1.png
    :align: middle

.. table::
    :align: center

    +---------------+---------------+
    |    Enabled    |    Disabled   |
    +===============+===============+
    | |enableseek|  | |disableseek| |
    +---------------+---------------+

Secondary display
=================

.. figure:: /images/more/settings/extra_video_sec_display.png
    :align: center
    :alt: Video Secondary display Setting

:guilabel:`Prefer clone` - When secondary displays are connected either via HDMI or Chromecast or any other methods, **VLC for Android** 
can be used as a remote to control video playback on secondary screen. Check this option to avoid remote control and clone the screen rather.
