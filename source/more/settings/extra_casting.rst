.. _doc-extra-casting:

*******
Casting
*******

**VLC for Android** can wireless cast to a secondary screen. 

.. role:: html-raw(raw)
    :format: html

.. figure:: /images/more/settings/extra_casting_general.png
    :align: center
    :alt: Casting Extra Settings

:guilabel:`Wireless casting` - Check this option to enable wireless casting from **VLC for Android**.

:html-raw:`<hr>`

:guilabel:`Audio Passthrough` - This feature lets your TV manage audio rendering. Disabling this setting will render audio on device.

:html-raw:`<hr>`

:guilabel:`Conversion quality` - You can choose conversion quality when casting to a remote screen. There are 4 options available.

.. figure:: /images/more/settings/extra_casting_general_quality.png
    :align: center
    :alt: Casting Conversion Quality
