.. _doc-android-more:

****
More
**** 

.. toctree::
    :name: toc-android-more
    :maxdepth: 1

    settings/index.rst
    network/index.rst
