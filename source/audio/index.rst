.. _doc-android-audio:

*****
Audio
*****

**VLC for Android** has a nice audio library manager and can play all popular audio formats. It supports audiobooks as well. You can directly set an audio as ringtone. 

.. toctree::
    :name: toc-android-audio
    :maxdepth: 1

    audio_explorer.rst
    audio_player.rst
