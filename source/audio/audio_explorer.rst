.. _doc-audio-explorer:

**************
Audio Explorer
**************

.. role:: html-raw(raw)
    :format: html

**VLC for Android** have a nice audio library manager. Audio files are arranged in 4 types of views, :html-raw:`<strong style="color:orange">ARTISTS</strong>` , :html-raw:`<strong style="color:orange">ALBUMS</strong>` , :html-raw:`<strong style="color:orange">TRACKS</strong>` and :html-raw:`<strong style="color:orange">GENRES</strong>` . An example is shown below.

.. |artists_view| image:: /images/audio/audio_explorer_artists.png
    :align: middle

.. |albums_view| image:: /images/audio/audio_explorer_albums.png
    :align: middle

.. |tracks_view| image:: /images/audio/audio_explorer_tracks.png
    :align: middle

.. |genres_view| image:: /images/audio/audio_explorer_genres.png
    :align: middle

.. table::
    :align: center

    +----------------+----------------+----------------+----------------+
    | ARTISTS        | ALBUMS         | TRACKS         | GENRES         |
    +================+================+================+================+
    | |artists_view| | |albums_view|  | |tracks_view|  | |genres_view|  |
    +----------------+----------------+----------------+----------------+

.. |kebab_menu| image:: /images/icons/ic_more.svg
    :width: 5%

At the top-right corner you can see many options for searching and sorting. Some options are available throughout the library. See below for their description. You can see all available options by tapping on *Options* (|kebab_menu|) at top-right corner.

.. |search_icon| image:: /images/icons/search.svg
    :width: 3%

.. |resume_play| image:: /images/icons/ic_shortcut_resume_playback.svg
    :width: 4%

.. |sort_icon| image:: /images/icons/ic_sort.svg
    :width: 5%

.. |list_icon| image:: /images/icons/ic_list.svg
    :width: 5%

.. |grid_icon| image:: /images/icons/ic_grid.svg
    :width: 5%

* |search_icon| :guilabel:`Search Media` - Tapping on this icon will open a search bar, you can search media that are currently indexed by **VLC for Android**.

* |resume_play| :guilabel:`Resume Playback` - Tapping this icon will resume last played audio.

* |sort_icon| :guilabel:`Sort by`  - You can sort your videos by *Name*, *Length*, *Recently Added*, *Date*, *Album Name*, and *Track*. Tap the option a second time to change the order(ascending/descending) of sorting.

* |grid_icon|/|list_icon| :guilabel:`Display in grid/Display in list` - Audios can be arranged either in list or grid.

* **Refresh** - Refresh the current media list.

:html-raw:`<hr>`

More Options
============

You can also access more options by tapping on *Options* (|kebab_menu|) in all views. All the available options are described below.

.. note:: You can tap a track in any view to play it. Once played you can open the audio player by tapping on the player strip at the   
    bottom of the screen. *Long tapping on the player strip will copy track information to the clipboard*.


.. |play| image:: /images/icons/ic_am_play.svg
    :width: 5%

.. |play_all| image:: /images/icons/ic_ctx_play_all_normal.svg
    :width: 5%

.. |append| image:: /images/icons/ic_am_append.svg
    :width: 5%

.. |information| image:: /images/icons/ic_am_information.svg
    :width: 5%

.. |insert_next| image:: /images/icons/ic_ctx_play_next_normal.svg
    :width: 5%

.. |add_playlist| image:: /images/icons/ic_am_addtoplaylist.svg
    :width: 5%

.. |delete| image:: /images/icons/ic_menu_delete.svg
    :width: 5%

.. |share| image:: /images/icons/ic_am_share_normal.svg
    :width: 5%

.. |set_ringtone| image:: /images/icons/ic_am_ringtone.svg
    :width: 5%

.. |stop_after| image:: /images/icons/ic_ctx_stop_after_this.svg
    :width: 5%

.. |remove_from_queue| image:: /images/icons/ic_ctx_remove_from_playlist_normal.svg
    :width: 5%

* |play| **Play** - Play/Play all audio in the selected *Artist*, *Album* or *Genre*. 
* |play_all| **Play all** - Play all tracks one by one.
* |append| **Append** - Append the audio/artist/album/genre to current play queue.
* |insert_next| **Insert next** - Insert the audio/artist/album/genre just after the the current audio in the play queue.
* |information| **Information** - Show information about current artist/album/track/genre. It includes *File size*, *Bitrate*, *Audio Codec*, *Number of Channels*, *Sample rate*, *Length*, *Number of tracks*, *Album name*, *Artist name*, *Genre name* and *Subtitle tracks(for audiobooks only)*.
* |add_playlist| **Add to playlist** - Add the current artist/album/track/genre to existing/new playlist.
* |set_ringtone| **Set as ringtone** - Set the current track as device ringtone.
* |stop_after| **Stop after this track** - Stop playback after this track.
* |remove_from_queue| **Remove** - Remove the track from current play queue.
* |delete| **Delete** - Delete the current selection. 
* |share| **Share** - This option opens a pop-up to share the current track via Social Media, Email, Wi-Fi, bluetooth etc.

:html-raw:`<hr>`

You can also see some or all of these options while in ActionMode. ActionMode is launched when you *tap and hold/long tap* artist/album/track/genre.

:html-raw:`<hr>`

.. |shuffle_play| image:: /images/icons/ic_auto_shuffle_circle.svg
    :width: 5%

You can also see a floating button |shuffle_play|. You can tap this button to start shuffle play all your tracks.
