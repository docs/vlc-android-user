.. _doc-audio-player:

************
Audio Player
************

.. role:: html-raw(raw)
    :format: html

Audio Player Notification
=========================

When a track is playing you can see a player strip at the bottom of the screen in the application and a player widget in the notification area. 

.. figure:: /images/audio/audio_player_notification.png
    :align: center
    :alt: Audio Player Notification

.. |play_icon| image:: /images/icons/ic_am_play.svg
    :width: 5%

.. |pause_icon| image:: /images/icons/ic_player_pause.svg
    :width: 5%

.. |player_next| image:: /images/icons/ic_player_next.svg
    :width: 5%

.. |player_previous| image:: /images/icons/ic_previous.svg
    :width: 5%

You can control audio playback from the notification even when screen is locked(most devices allow this action, check permissions if you can't access notifications when screen is locked). You have following controls available in the **VLC for Android** audio player notification.

* You can seek audio by sliding your finger across the progress bar. 
* |play_icon|/|pause_icon| - Play/pause the track.
* |player_previous|/|player_next| - Switch to previous/next track in the play queue. 
* You also get information about *Track title*, *Album name*, *Track cover art*, *Artist name*, and *How long VLC has been playing*.

.. note:: To dismiss **VLC for android** notification, pause/stop the playback and then you can dismiss it.

:html-raw:`<hr>`

To open the audio player you can play a track then tap at the player strip at the bottom of screen or tap on the notification. When audio 
player is launched for the first time, :html-raw:`<strong style="color:orange">Audio player tips</strong>` are shown. 

Audio Player Strip
==================

.. figure:: /images/audio/audio_player_tips.png
    :align: center
    :alt: Audio Player Tips

.. |rightleft_gesture| image:: /images/icons/ic_gesture_swipe_horizontally.svg
    :width: 10%

.. |updown_gesture| image:: /images/icons/ic_gesture_swipe_vertically.svg
    :width: 5%

.. |tap_gesture2| image:: /images/icons/ic_gesture_finger_2.svg
    :width: 5%

.. |tap_gesture3| image:: /images/icons/ic_gesture_finger_3.svg
    :width: 5%

On the player strip at the bottom of the screen you can use following controls:-

* |rightleft_gesture| Hold and swipe left/right on the player strip to switch to previous/next track in the play queue.
* |tap_gesture2| Tap on the player strip to open current play queue.
* |tap_gesture3| Tap on |play_icon|/|pause_icon| to play/pause the audio. You can also hold the icon to stop the playback. 

Tap :html-raw:`<strong style="color:white;background-color:orange">Got it, dismiss this</strong>` to close the tips.

:html-raw:`<hr>`

Audio Player Queue
==================

On tapping the audio player strip, the audio player queue is opened. When opening first time :html-raw:`<strong style="color:orange">Playlist tips</strong>` are shown.

.. figure:: /images/audio/audio_player_queue_tips.png
    :align: center
    :alt: Audio Player Tips

In the player queue you can use following controls:-

* |rightleft_gesture| Hold and swipe left/right on a track to remove it from the play queue.
* |updown_gesture| Hold and swipe up/down on a track to move it above/below in the play queue.
* |tap_gesture3| Tap on |player_previous|/|player_next| to switch to previous/next track in the play queue.

Tap :html-raw:`<strong style="color:white;background-color:orange">Got it, dismiss this</strong>` to close the tips.

:html-raw:`<hr>`

Audio Player
============

Audio player looks like the screenshot below. You can control blurred cover background in :ref:`doc-extra-interface-settings` settings under *Audio* section. 

.. figure:: /images/audio/audio_player.png
    :align: center
    :alt: Audio Player

.. |search_icon| image:: /images/icons/search.svg
    :width: 3%

.. |playlist| image:: /images/icons/ic_playlist.svg
    :width: 5%

.. |kebab_menu| image:: /images/icons/ic_more.svg
    :width: 5%

.. |auto_repeat| image:: /images/icons/ic_auto_repeat.svg
    :width: 5%

.. |auto_repeat_1| image:: /images/icons/ic_auto_repeat_one.svg
    :width: 5%

.. |shuffle| image:: /images/icons/ic_shuffle.svg
    :width: 5%

To hide the audio player/play queue tap on the track title at the top. At the top-right corner you can see *Search* (|search_icon|), *Play Queue* (|playlist|) and *Options* (|kebab_menu|). At the top there are control buttons. 

* |search_icon| :guilabel:`Search Media` - Tapping on this icon will open a search bar, you can search media that are currently indexed by **VLC for Android**. 
* |playlist| :guilabel:`Play queue` - Tap on this icon to switch between Audio Player and Play Queue.
* |shuffle| :guilabel:`Shuffle` - Start a shuffle play. The icon will turn orange.
* |player_previous|/|player_next| - Tap to switch to previous/next track in the play queue.
* |play_icon|/|pause_icon| - Play/pause the track.
* |auto_repeat| :guilabel:`Auto repeat` - Tapping this icon one time will change its color to orange and the audio will be repeated indefinitely. To repeat the audio just one more time tap the icon again. It should change to |auto_repeat_1| then. 
* |kebab_menu| Tap on **Options** to see more features/controls.

More Options
============

.. figure:: /images/audio/audio_player_more_options.png
    :align: center
    :alt: Audio Player More Options

.. |sleep_timer| image:: /images/icons/ic_sleep.svg
    :width: 5%

|sleep_timer| :guilabel:`Sleep timer` - You can set a sleep timer to let **VLC for Android** automatically close audio playback after 
given period of time. 

.. figure:: /images/audio/audio_player_sleep_timer.png
    :align: center
    :alt: Sleep Timer

:html-raw:`<hr>`

.. |playback_speed| image:: /images/icons/ic_speed.svg
    :width: 5%

|playback_speed| :guilabel:`Playback speed` - You can set playback speed by using the slider, or by using arrows in the range 0.25x to 4x.

.. figure:: /images/audio/audio_player_playback_speed.png
    :align: center
    :alt: Playback Speed

:html-raw:`<hr>` 

.. |jump_time| image:: /images/icons/ic_jumpto.svg
    :width: 5%

|jump_time| :guilabel:`Jump to time` - This feature lets you jump to arbitrary time in the audio. Alternatively you can also seek using 
gestures or audio progress bar.

.. figure:: /images/audio/audio_player_jump_time.png
    :alt: Jump To Time
    :align: center

:html-raw:`<hr>`

.. |equalizer| image:: /images/icons/ic_equalizer.svg
    :width: 5%

|equalizer| :guilabel:`Equalizer` - **VLC for Android** embeds a powerful audio equalizer to customize your audio experience. By 
default the equalizer is disabled. To enable it, toggle the button at top-right corner. There are a lots of presets included and you can 
also create your own preset. 

.. figure:: /images/audio/audio_player_equalizer.png
    :align: center
    :alt: Equalizer

:html-raw:`<hr>`

.. |audiobook_chapter| image:: /images/icons/ic_chapter.svg
    :width: 5%

|audiobook_chapter| :guilabel:`Go to chapter...` - This option is only visible if an audiobook is played. You can jump to any chapter listed.

:html-raw:`<hr>`

.. |ab_repeat| image:: /images/icons/ic_abrepeat.svg
    :width: 5%

.. |ab_repeat_reset| image:: /images/icons/ic_abrepeat_reset.svg
    :width: 5%

.. |ab_marker| image:: /images/icons/ic_abrepeat_marker.svg
    :width: 5%

.. |ab_marker_reset| image:: /images/icons/ic_abrepeat_reset_marker.svg
    :width: 5%

|ab_repeat| :guilabel:`A-B repeat` - A-B repeat is to loop a audio between two points. To start an A-B repeat follow these steps.

* Tap |ab_repeat| :html-raw:`<strong style="color:orange">SET START POINT</strong>` to set a start marker |ab_marker| at the time you want to start this repeat.

.. figure:: /images/audio/audio_player_abrepeat_start.png
    :alt: Start AB Repeat
    :align: center

* Now tap |ab_repeat| :html-raw:`<strong style="color:orange">SET END POINT</strong>` to set an end marker |ab_marker| at the time you want to end this repeat. Once set the *A-B repeat* will start. 

.. figure:: /images/audio/audio_player_abrepeat_end.png
    :alt: End AB Repeat
    :align: center

* Tap on |ab_marker_reset| to remove markers.
* Tap on |ab_repeat_reset| to close *A-B repeat*

.. figure:: /images/audio/audio_player_abrepeat.png
    :alt: AB Repeat
    :align: center

:html-raw:`<hr>`

:guilabel:`Save Playlist` - Add this audio to an existing/new playlist.

.. figure:: /images/audio/audio_player_save_playlist.png
    :alt: Save Playlist
    :align: center
